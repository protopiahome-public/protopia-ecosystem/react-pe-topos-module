import { CompoundTag, Intent, Position, Tag } from "@blueprintjs/core";
import Moment from "react-moment";
import { Route, Routes, useNavigate, useParams } from "react-router";
import { Link } from "react-router-dom";
import { DIALOG_SIZE } from "src/libs/interfaces/layouts";
import { getFeedRoute, getRoute, schema } from "src/libs/layouts";
import { getURL } from "src/libs/scalars/scalars/URL";
import { ClipboardCopyBtn, Likes, ScalableDialog } from "src/libs/useful";
import Comments from "src/libs/useful/Comments";
import { __ } from "src/libs/utilities";
import { IFeedProps } from "src/modules/pe-basic-module/views/core/FeedComponent";
import { ITrimContent, trimContent } from "src/modules/pe-fest-module/views/utils/trimContent";
import { IAds } from "../../data/interfaces";
import AdCardMobile from "./AdCardMobile";

interface IProps extends IFeedProps { 
    item: IAds
    className?: string
    firstCommentAutofocus?: boolean
}

const AdCardScreen = ({item, className, ...props }:IProps) :JSX.Element => {
    const navigate = useNavigate() 
    let colClass = " col-12 " 
    if( props.params?.col )
    {
        switch(parseInt( props.params?.col ))
        {
            case 2:
                colClass = " col-md-6 "
                break;
            case 3:
                colClass = " col-md-4 "
                break;
            case 4:
                colClass = " col-md-3 "
                break;
            case 1:
            default:                
                colClass = " col-12 "
                break;

        }
    }
    const tr: ITrimContent = trimContent( item.post_content, 40 )
    const typeName = __( item.parent_type ? schema()[item.parent_type].name : "" )
    return <>
        <Link 
            className={ `position-relative  flex-centered flex-column ${ className } ${colClass} my-3 pointer hover ` } 
            to={ `${ item.id}` }
        >
            <div className="d-flex border border-secondary " style={{maxWidth:700, minHeight: 270}}>
                <div className="thumbnail h-100 position-relative w-50" style={{ backgroundImage: `url(${ item.thumbnail })`, }}>
                    <img src="http://fakeimg.pl/150x200" alt="" className="unvisibled h-100" style={{width: "auto"}}/> 
                    {
                        !!item.parent?.id && 
                            <CompoundTag  
                                // round
                                interactive
                                active={false}
                                intent={Intent.PRIMARY}
                                className="position-absolute m-1 bottom right " 
                                leftContent={ <div className="small text-overflow">{typeName}</div> }
                                
                            > 
                                <div className="small text-overflow">
                                    { item.parent?.title}
                                </div>
                            </CompoundTag>
                    }
                    {
                        !!item.date &&
                            <Moment  locale="ru" format="D.MM.YYYY" className={`px-3 py-1 bg-secondary text-light position-absolute top left m-1`}>
                                { parseInt(item.date.toString()) * 1000 }    
                            </Moment> 
                    }
                </div>
                <div className="position-relative w-50"> 
                    <div className="bg-secondary-light p-3 h lead text-force-contrast" style={{fontWeight: 900, lineHeight:0.9}}>
                        {item.title}
                    </div>
                    
                    {
                        tr.isTrim 
                            ?
                            <div className={`pointer m-3 text-force-contrast `} dangerouslySetInnerHTML={{__html: tr.content}} /> 
                            :
                            <div className={`m-3 text-force-contrast `} dangerouslySetInnerHTML={{__html: tr.content}} />  
                    }
                    <div className="d-flex align-items-center position-absolute bottom left" onClick={(evt) => { evt.stopPropagation(); evt.preventDefault(); }} >
                        <ClipboardCopyBtn
                            data={`${window.location.origin}/${getRoute(getFeedRoute("Ad"))}/${item.id}`}
                            type="icon"
                            label={__("Copy link")}
                            position={Position.TOP_RIGHT}
                        /> 
                        <Link to={ `${ item.id}` } >
                            <Comments item={item } dataType="Ad" />
                        </Link> 
                        {/* <Likes item={item} dataType="Ad" className="" /> */}
                    </div>
                    {
                        item.url &&
                        <a href={ getURL(item.url)[0]} target="_blank" rel="noreferrer" className="position-absolute m-1 bottom right">
                            <Tag intent={Intent.SUCCESS}>
                                <small>{ getURL(item.url)[1] || __("Ins")}</small>
                            </Tag>
                        </a> 
                    } 
                </div>
            </div>
        </Link>
        <Routes>
            <Route 
                path="/:adId" 
                element={
                    <AdContent 
                        {...props} 
                        item={item} 
                        className={className} 
                        parentRoute={ `/${getRoute(getFeedRoute("Ad"))}` } 
                    />
                } 
            />
        </Routes> 
    </>
}

export default AdCardScreen

interface IAdContent extends IProps{
    parentRoute: string
}
const AdContent = (props: IAdContent): JSX.Element => {
    const navigate = useNavigate()
    const {adId} = useParams()

    const onClose = () => {
        navigate(props.parentRoute)
    }
    return adId
        ?
        <ScalableDialog
            dialogSize={DIALOG_SIZE.MINIMAL}
            isOpen={true}
            onClose={onClose}
            hideClose={false}
            isCloseButtonShown
        >
            <div className="py-4">
                <div className="max-height overflow-y-auto">
                    <AdCardMobile {...props} firstCommentAutofocus />
                </div>   
            </div> 
        </ScalableDialog>
        :
        <></>
}