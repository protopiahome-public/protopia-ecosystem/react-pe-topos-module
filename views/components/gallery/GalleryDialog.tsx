import { Button, Dialog, Icon } from "@blueprintjs/core" 
import DialogRight from "./DialogRight"
import { IState, useMainStore } from "src/settings/zustand"

export interface IGallertDialog {
    isOpen: boolean
    data: any[]
    i: number
    setI: (i:number) => void
    setIsOpen: (isOpen: boolean) => void

}
export const GalleryDialog = ({ isOpen, data, i, setI, setIsOpen }: IGallertDialog) : JSX.Element => { 
    const isGlleryFullSize = useMainStore((state: IState) => state.isGlleryFullSize) 
    return <>
        <Dialog
            lazy={ false } 
            isOpen={ isOpen } 
            portalClassName="overflow-hidden"
            className="full flex-centered transparent flex-row"
        >
            <div className="prev ">
                <Button  
                    onClick={() => setI( i > 0 ? i-1 : data.length - 1 )} 
                    className="bg-dark-secondary hover-straght" 
                >
                    <Icon icon="chevron-left" size={ 33 } color="#FFFFFF" />
                </Button>
            </div>    
            <div className=" position-relative gallery-dialog">
                <img 
                    src={ Array.isArray( data ) && data[i] } 
                    alt=""  
                    className={ `gallery-dialog-thumbnail ${ isGlleryFullSize ? "full-size" : "" }` } 
                />
            </div>
            <DialogRight 
                data={ data } 
                i={ i } 
                setI={ setI } 
                setIsOpen={ setIsOpen }
            />
        </Dialog>
    </>
} 

export default GalleryDialog