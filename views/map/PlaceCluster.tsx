import { Button, ButtonGroup, Intent } from "@blueprintjs/core"
import { ITeam, IEvent, IPlace, All } from "../../data/interfaces"
import { useState } from "react"
import PlaceContent from "../components/PlaceContent"
import { placeTypes } from "../../data/mocks/placeTypes"
import { LayoutIcon } from "src/libs/useful"

interface IProps {
    items: All[]
    onClose: () => void
    onOpenLogin: () => void
}
const PlaceCluster = (props:IProps) :JSX.Element => {
    const [current, setCurrent] = useState<All>(props.items[0] )
    return (
        <div className="w-100 h-100 d-flex">
            <div className="topos-dmap-dialog-menu overflow-y-auto">
                <ButtonGroup vertical className="w-100">                    
                {
                    props.items.map((item, i) => {
                        const placeType = placeTypes().filter(pt => pt.id === item.type[0])[0] || placeTypes()[0]
                        return <Button 
                            minimal={current.id !== item.id} 
                            fill 
                            className="py-3 px-3 text-light text-overflow d-flex"
                            alignText="right" 
                            key={item.id}
                            intent={current.id === item.id ? Intent.DANGER : Intent.NONE}
                            onClick={() => setCurrent(props.items[i])}
                        >
                            <span>{item.title}</span> 
                        </Button>
                    })
                }
                </ButtonGroup>
            </div>
            <div className="topos-dmap-dialog-content">
                <PlaceContent item={current as IPlace} onClose={props.onClose} onOpenLogin={props.onOpenLogin}/>
            </div>
        </div>
    )
}

PlaceCluster.propTypes = {}

export default PlaceCluster