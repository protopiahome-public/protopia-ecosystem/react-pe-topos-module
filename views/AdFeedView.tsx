import React from "react" 
import BasicState from "src/libs/basic-view"
import AdFeedForm from "./ads/AdFeedForm" 
import "../assets/css/style.scss"

export default class AdFeedView extends BasicState {
    props: any
    addRender = () => {
        return <AdFeedForm {...this.props} />  
    }
}