
import { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
// import CabinetPhaseForm from './CabinetPhaseForm'
import switchToBlog from 'src/modules/pe-basic-module/hooks/switchToBlog'
import { useFestDataStore } from 'src/modules/pe-fest-module/data/store'
import { useFestival } from 'src/modules/pe-fest-module/hooks/festival'
import { useMainStore } from 'src/settings/zustand'
import { ID } from 'src/libs/interfaces/layouts'
import { __ } from 'src/libs/utilities'
import { getAdminRoutes } from '../../data/utils/getAdminRoutes'

const Cabinet = (props:any):JSX.Element => { 
    
    const allRoutes = getAdminRoutes()
    const [blocks, setBlocks] = useState<string[]>([])
    useEffect(() => {
        let _blocks: string[] = [] 
        Object.keys(allRoutes ).forEach(b => {
            if(! _blocks.includes( allRoutes[b].group )) {
                _blocks.push( allRoutes[b].group )
            }
        })
        setBlocks(_blocks) 
    }, [])

    const params = useParams()
    const festId: ID = params.landId || "-1"
    const isLoading = useFestival( true, festId )
    useEffect(() => {
        useFestDataStore.setState({ festId })
        useMainStore.setState({landId: festId})
        switchToBlog( festId )
    }, [festId]) 

    
    return <>
        {/* <CabinetPhaseForm /> */}
        {
            blocks.map(block => {
                const mine = Object.keys(allRoutes).filter( (r: string) =>  allRoutes[r].group === block )
                //console.log( mine )
                const icon = mine[0]
                    ?
                    allRoutes[mine[0]].icon
                    :
                    <></>
                const rightWidget: JSX.Element = mine[0]
                    ?
                    allRoutes[mine[0]].rightMenuWidget || <></>
                    :
                    <></>
                return <div className='card mb-4' key={ block }>
                    <div className="card-body p-0">
                        <div className="d-flex justify-content-between align-items-center flex-wrap border-bottom-1 border-secondary ">
                            <h5 className='card-title pt-3 pl-4 pb-1'>
                                <span className="mr-3">{icon}</span> {__( block )}
                            </h5>
                            <div className="pr-4 px-1">
                                { rightWidget }
                            </div> 
                        </div>
                        <div className='w-100'>
                            <Block mine={mine} />
                            {/* <div className="d-flex mr-auto">
                                
                            </div> 
                            <div className="d-flex">
                                <Block mine={mine} position="right" />
                            </div>  */}
                        </div>
                    </div>
                </div> 
            })
        }   
    </>

}

export default Cabinet 

interface IBlokProps {
    mine: any[] 
}
const Block = (props: IBlokProps) => {
    const ug: any = {}
    const allRoutes = getAdminRoutes()
    props.mine
        .forEach((r: string) => {
            ug[ allRoutes[ r ].undergroup ] = [
                ...( ug[ allRoutes[ r ].undergroup ] || [] ),
                { ...allRoutes[ r ], route: r }
            ]
        })

    // console.log(ug);
    
    return Object.keys(ug)
        .map( (key, i) => {
            return <div key={key}>
                {
                    key !== "undefined" && <div className="pt-2 pl-4 ">
                        <div className='lead h'>{ __(key) }</div>
                        <div className="d-flex">
                        {
                            ug[key]
                                .filter((item: any) => item.position === "head" )
                                .map( (item: any) => {
                                    const ElComponento = item.component
                                    return <ElComponento />
                                })
                        }
                        </div>
                    </div>
                }
                <div className={`d-flex justify-content-between align-items-center flex-wrap px-4 py-3 ${ i < Object.keys(ug).length - 1 ? "border-bottom-1 border-secondary-light " : "" } `}>
                    <div className="d-flex">
                    {
                        ug[key]
                            .filter((item: any) => item.position === "left" )
                            .map( (item: any) => {
                                // const item: any = allRoutes[ r ] 
                                const className: string = item.position === "left"
                                    ?
                                    "btn-outline-secondary"
                                    :
                                    "btn-link"
                                return <NavLink 
                                    to={ item.route } 
                                    className={`btn btn-sm  text-force-contrast mr-1 mb-1 ${className} `}
                                    key={ item.route } 
                                >
                                    {__( item.title )}
                                </NavLink> 
                            })
                    } 
                    </div> 
                    <div className="d-flex">
                    {
                        ug[key]
                            .filter((item: any) => item.position === "right" )
                            .map( (item: any) => {
                                // const item: any = allRoutes[ r ] 
                                const className: string = item.position === "left"
                                    ?
                                    "btn-outline-secondary"
                                    :
                                    "btn-link"
                                return <NavLink 
                                    to={ item.route } 
                                    className={`btn btn-sm  text-force-contrast mr-1 mb-1 ${className} `}
                                    key={ item.route } 
                                >
                                    {__( item.title )}
                                </NavLink> 
                            })
                    } 
                    </div> 
                </div> 
            </div>
        }) 
}
