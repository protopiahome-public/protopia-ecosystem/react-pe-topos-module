import React from "react" 
import BasicState from "src/libs/basic-view"
import EventForm from "./event/EventForm" 
import "../assets/css/style.scss"

export default class EventView extends BasicState {
    props: any
    addRender = () => {
        return <EventForm {...this.props} />  
    }
}