

import { LayoutIcon } from "src/libs/useful"
import { getContrastColor } from "src/libs/utilities"
import { ITeam, ITeamType } from "../../data/interfaces"

interface IProps {
    item: ITeam
    className: string
}
const TeamTypeLabel = ({item, className}: IProps) :JSX.Element => {
    return <div className={`d-flex ${ className}`}>
        {
            item.teamTypes.map((pt: ITeamType) => {
                return <div 
                    className=" m-1 d-flex align-items-center rounded-pill pointer" 
                    key={pt.id} 
                    style={{background:pt.color}}
                >
                    <span className="topos-placetype-label-icon">
                        <LayoutIcon
                            src={ pt.icon }
                            className=""
                            style={{ width:16, height: 16 }}
                        />
                    </span>
                    <span className="px-3 py-1" style={{ color: getContrastColor(pt.color) }}>
                        {pt.title}
                    </span>
                </div>
            })
        }
    </div> 
} 

export default TeamTypeLabel