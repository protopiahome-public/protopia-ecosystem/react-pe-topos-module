import React from "react" 
import BasicState from "src/libs/basic-view"
import ToposAficheForm from "./afiche/ToposAficheForm" 

export default class ToposAficheView extends BasicState {
    props: any
    addRender = () => {
        return <ToposAficheForm {...this.props} />  
    }
}