import switchToBlog from "src/modules/pe-basic-module/hooks/switchToBlog"
import { clearFestival } from "src/modules/pe-fest-module/hooks/festival"
import { ID } from "@/libs/interfaces/layouts"

function useEvent( id: ID = "-1" ) {
    clearFestival()
    switchToBlog( id )
}
export default useEvent