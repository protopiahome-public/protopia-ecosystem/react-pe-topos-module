# React PE Topos module

<p align="center">
  <a href="https://nact.xyz/" target="blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/51646262/pe_logo_topos.jpg" width="100" alt="Ludens Logo" /></a>
</p> 
 


 Модуль для [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/). Включает в себя набор Экранов, Виджетов, настроек и ассетов для сервиса ТОПОС. Симулирует функционал социальных сервисов...

![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)

## Подробное описание:  



[см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe)

## Установка
 

## Структура и сценарии использования:

 [см.](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/admin-module-views/)
